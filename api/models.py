from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models, transaction


class TimedBaseModel(models.Model):
    class Meta:
        abstract = True

    id = models.AutoField(primary_key=True, verbose_name='Id колонки')
    created_at = models.DateTimeField(auto_now=True)


class UserManager(BaseUserManager):

    def _create_user(self, username, password, **extra_fields):
        """
        Creates and saves a User with the given email,and password.
        """
        if not username:
            raise ValueError('The given login must be set')
        try:
            with transaction.atomic():
                user = self.model(username=username, **extra_fields)
                user.set_password(password)
                user.save(using=self._db)
                return user
        except:
            raise

    def create_user(self, username, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, password, **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        return self._create_user(username, password=password, **extra_fields)


class User(AbstractUser, TimedBaseModel):
    DSP_ROLE = 0
    MACHINE_ROLE = 1
    ROLE_CHOICES = (
        (DSP_ROLE, "ДСП"),
        (MACHINE_ROLE, "ТЧМ")
    )
    username = models.CharField(verbose_name="Имя пользователя", max_length=100, unique=True)
    first_name = models.CharField(verbose_name="Имя сотрудника", max_length=55)
    middle_name = models.CharField(verbose_name="Фамилия сотрудника", max_length=55, blank=True, null=True)
    is_staff = models.BooleanField(default=False, verbose_name="Сотрудник?")
    is_active = models.BooleanField(default=True, verbose_name="Активен?")
    role = models.SmallIntegerField(choices=ROLE_CHOICES, verbose_name="Роль", blank=True, null=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.username
