from django.shortcuts import render
from rest_framework import permissions, parsers, status
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken
from .models import User

from .serializer import AuthSerializer


class AuthView(APIView):
    serializer_class = AuthSerializer
    permission_classes = [permissions.AllowAny]

    @staticmethod
    def post(request, *args, **kwargs):
        serializer = AuthSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            return serializer.check_success_user(serializer.validated_data)
