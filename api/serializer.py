from rest_framework import serializers, status
from rest_framework.response import Response

from .models import User
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken


class AuthSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=55)
    password = serializers.CharField(max_length=55)

    @staticmethod
    def check_success_user(data):
        user = User.objects.filter(
            username=data.get('username')
        ).first()
        if user is not None:
            if user.check_password(data.get("password")):
                access_token = AccessToken.for_user(user)
                refresh_token = RefreshToken.for_user(user)
                return Response({"success": True, "access_token": str(access_token), "refresh_token": str(refresh_token)})
            else:
                return Response({"success": False, "message": f"Неверный пароль для пользователя {user.username}"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"success": False, "message": "Пользователь не найден"},
                            status=status.HTTP_400_BAD_REQUEST)

